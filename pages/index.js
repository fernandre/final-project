import Layout from "../components/layouts/Layout";
import Category from "../components/frontpage/Catagory";
import Gameselection from "../components/frontpage/Gameselection";
import Slidevent from "../components/frontpage/Slidevent";
import {Container} from "reactstrap";

export default function LandingPage() {
  return (
    <Layout title="Gaming Platform">
      <Container className="pt-4">
        <Slidevent></Slidevent>
        <Category></Category>
        <Gameselection></Gameselection>
        {/* <About></About> */}
      </Container>
    </Layout>
  );
}
