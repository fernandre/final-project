import Layout from "../../../components/layouts/Layout";
import dynamic from "next/dynamic";

const PDFComponent = dynamic(() => import("../../../components/profile/media/PDFViewer"), {
  ssr: false
});

export default function views() {
  return (
    <>
      <Layout title="PDF Reader">
        <div className="pt-3">
          <center>
            <PDFComponent />
          </center>
        </div>
        <br />
        <br />
        <br />
      </Layout>
    </>
  )
}
