Dibuat oleh KELOMPOK 1 Binar FSW wave 16
Anggota kelompok terdiri dari:

1. Cipto Widarto
2. Athalia Beatrice Boedianto
3. Fernandre Kurniawan

## Role

1. Koordinator / Scrum Master : Cipto Widarto
2. Repo Maintainer : Fernandre Kurniawan
3. Member : Athalia Beatrice Boedianto

## Deskripsi Kerja

1. Cipto Widarto

   - handling media PDF
   - setup unit testing
   - switch color mode

2. Fernandre Kurniawan

   - setup eslint
   - setup CI/CD

3. thalia Beatrice Boedianto

   - handling media Video

## Disclaimer

- unit testing mengunakan @testing-library-react & jest

## coverage report

-----------------------------|---------|----------|---------|---------|-----------------------------------------------
File                         | % Stmts | % Branch | % Funcs | % Lines | Uncovered Line #s
-----------------------------|---------|----------|---------|---------|-----------------------------------------------
All files                    |   61.63 |    37.56 |   47.45 |   61.14 |                                               
 components/auth             |   96.33 |     87.5 |    92.3 |   96.33 |                                               
  Content.js                 |     100 |      100 |     100 |     100 |                                               
  FormLogin.js               |   94.11 |     87.5 |   85.71 |   94.11 | 35,96
  FormRegister.js            |   94.87 |     87.5 |    87.5 |   94.87 | 42,96
  SignFacebook.js            |     100 |      100 |     100 |     100 | 
  SignGoogle.js              |     100 |      100 |     100 |     100 | 
 components/frontpage        |   52.38 |      100 |      25 |   52.38 | 
  Catagory.js                |      42 |      100 |   17.64 |      42 | 16-20,30-34,37-41,44-48,51-55,58-62,65-69,104
  Gameselection.js           |     100 |      100 |     100 |     100 | 
  Slidevent.js               |   88.88 |      100 |      50 |   88.88 | 9
 components/games            |   79.62 |    53.84 |   88.88 |   79.62 |                                               
  CardUser.js                |   70.37 |       50 |     100 |   70.37 | 21-29
  Cardinfo.js                |   84.21 |    33.33 |      75 |   84.21 | 44-47
  ImgDetail.js               |     100 |      100 |     100 |     100 | 
 components/home             |   64.38 |        0 |    37.5 |   70.14 | 
  LastPlayedGame.js          |   58.06 |        0 |   28.57 |   64.28 | 31-33,37-39,43-44,48,52
  Leaderboards.js            |     100 |      100 |     100 |     100 | 
  Statistics.js              |     100 |      100 |     100 |     100 | 
  Updates.js                 |   58.06 |        0 |   28.57 |   64.28 | 18-20,24-26,30-31,35,39
 components/layouts          |   68.42 |    44.11 |      70 |   68.42 | 
  Footer.js                  |     100 |      100 |     100 |     100 | 
  Layout.js                  |     100 |       75 |     100 |     100 | 12-19
  NavbarComponent.js         |      64 |    27.27 |    62.5 |      64 | 24-30,34-35,38,45,48-54,59,152
 components/list             |   88.23 |     12.5 |      75 |   88.23 | 
  Content.js                 |   85.71 |     12.5 |   71.42 |   85.71 | 23-26,41
  DetailList.js              |     100 |      100 |     100 |     100 | 
 components/profile          |   76.08 |    29.41 |      80 |   76.08 | 
  Content.js                 |     100 |      100 |     100 |     100 | 
  GameRPS.js                 |   76.47 |       50 |      60 |   76.47 | 15-18,41
  UserDetail.js              |   74.07 |    26.66 |     100 |   74.07 | 14-15,24-27,31-32
 components/profile/media    |   56.25 |       50 |   14.28 |      60 | 
  PDFViewer.js               |   56.25 |       50 |   14.28 |      60 | 12-29
 components/profile/updates  |   44.94 |    31.11 |   26.31 |   44.94 | 
  UpadatePhoto.js            |   35.13 |    33.33 |   14.28 |   35.13 | 23-24,29-31,35-36,39-40,44-98,103-124         
  UpdateEmailComponent.js    |   47.05 |    16.66 |      25 |   47.05 | 15-16,21-22,26-27,30-31,35-49
  UpdateName.js              |   58.33 |       50 |      50 |   58.33 | 15-16,21-23,41-60
  UpdatePasswordComponent.js |      50 |    16.66 |      25 |      50 | 16-17,22-23,27-28,31-32,36-49
 firebase                    |     100 |      100 |     100 |     100 | 
  config.js                  |     100 |      100 |     100 |     100 | 
 redux/actions               |   34.88 |     8.33 |   14.28 |   27.27 | 
  authActions.js             |   33.33 |     8.33 |   33.33 |   27.27 | 21-81,88-93,99-150,174-209,236-270,281       
  profileAction.js           |   38.46 |      100 |       0 |   27.27 | 13-38,44-69,75-97,103-125
 redux/reducers              |   70.73 |    47.82 |     100 |   70.73 |
  authReducer.js             |   76.92 |       70 |     100 |   76.92 | 11,24-31
  gameReducer.js             |      50 |    28.57 |     100 |      50 | 10-30
  profileReduser.js          |      60 |    33.33 |     100 |      60 | 11-26
  rootReducer.js             |     100 |      100 |     100 |     100 |
 redux/reduxtest             |     100 |        0 |     100 |     100 |
  renderWithState.js         |     100 |        0 |     100 |     100 | 13
-----------------------------|---------|----------|---------|---------|-----------------------------------------------
Test Suites: 28 passed, 28 total
Tests:       40 passed, 40 total
Snapshots:   0 total
Time:        16.32 s
Ran all test suites.


