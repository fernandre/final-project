import React from "react";
import {useEffect, useState} from "react";

const VideoDetail = ({detailGame, gameId}) => {
  const [trailer, setTrailer] = useState("");

  useEffect(() => {
    setTrailer(detailGame);
  }, [detailGame, gameId]);

  return (
    <>
      <div className="iframe-container center player"  >
        <iframe className="responsive-iframe"
          title="Cloud Hosted Video Player"
          src={trailer.video}
          alt={trailer.name}
          width="90%"
          height="350"
          allow="autoplay; fullscreen; encrypted-media; picture-in-picture"
          allowFullScreen
          frameBorder="0"
        ></iframe>
      </div>

    </>
  );
}

export default VideoDetail;