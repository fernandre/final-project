import {useEffect, useState} from "react";
import Image from "next/image";

const ImgDetail = ({detailGame, gameId}) => {
  const [data, setData] = useState("");

  useEffect(() => {
    setData(detailGame);
  }, [detailGame, gameId]);

  return (
    <div className=" d-flex justify-content-center">
      {data.thumbnail != null && (
        <Image className="img-fluid mt-4" src={data.thumbnail} alt={data.name} width={350} height={350} />
      )}
    </div>
  );
};

export default ImgDetail;
