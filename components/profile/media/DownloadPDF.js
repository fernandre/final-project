import {Button} from 'reactstrap';
import html2canvas from 'html2canvas';
import jsPDF from 'jspdf';
import {useEffect, useState} from 'react';
import Link from "next/link";

export default function DownloadPDF() {
  const [dataUser, setDataUser] = useState("")
  useEffect(() => {
    const token = localStorage.getItem("token");
    if (token) {
      const user = JSON.parse(localStorage.getItem("user"));
      setDataUser(user);
    }
  }, [])

  const printDocument = () => {
    const input = document.getElementsByClassName('invoicePages');

    const pdf = new jsPDF();

    for (let i = 0;i < input.length;i++) {
      html2canvas(input[i]).then((canvas) => {
        // console.log(canvas);
        const imgData = canvas.toDataURL('image/png');
        pdf.addImage(imgData, 'JPEG', 0, 0);
        // pdf.addImage(imgData, 'JPEG', 0, 0, 210, 297, 'pdf', 'NONE', 0);
        if (input.length - 1 === i) {
          pdf.save('my-profile.pdf');
        } else {
          pdf.addPage();
        }
      });
    }
  }

  return (
    <div>
      <div className="mt-2 text-center">
        <Link href={"/profile/update/" + dataUser.uid} passHref>
          <Button className="mx-3" color="primary">Update Profile</Button>
        </Link>
        <Link href="/profile/media/views" passHref>
          <Button className="mx-3" color="success">PDF Reader</Button>
        </Link>
        <Button className="mx-3" color="warning" onClick={printDocument}>Print Profile</Button>
      </div>
      <div className='text-center my-3'>
      </div>
      {/* PAGE 1 */}
      {/* <div className="invoicePages" id="invoicePageOne"> */}
      {/* </div> */}
      {/* PAGE 2 */}
      {/* <div className="invoicePages" id="invoicePageTwo"> */}
      {/* </div> */}
    </div>
  );
}