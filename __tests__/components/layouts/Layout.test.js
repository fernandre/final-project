import Layout from "../../../components/layouts/Layout";
import {screen} from "@testing-library/react";
import {renderWithState} from "../../../redux/reduxtest/renderWithState";
import "@testing-library/jest-dom";

beforeAll((done) => {
  done()
});
afterAll((done) => {
  done()
});
describe("Test <Layout /> component rendering", () => {
  test("Test", async () => {
    try {
      const {getByText} = renderWithState(<Layout />, {
        initialState: {
          auth: {
            initState: {
              scoreRedux2: "0"
            }
          }
        }
      });
      getByText("Gaming Platform")
      expect(screen.getByText("Gaming Platform")).toBeVisible();
    } catch (e) {
      expect(e).toMatch('error');
    }
  });
});