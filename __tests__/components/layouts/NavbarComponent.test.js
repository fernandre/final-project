import NavbarComponent from "../../../components/layouts/NavbarComponent";
import {screen} from "@testing-library/react";
import {renderWithState} from "../../../redux/reduxtest/renderWithState";
import "@testing-library/jest-dom";

beforeAll((done) => {
  done()
});
afterAll((done) => {
  done()
});
describe("Test <NavbarComponent /> component rendering", () => {
  test("Test", async () => {
    try {
      const {getByText} = renderWithState(<NavbarComponent />, {
        initialState: {
          auth: {
            initState: {
              scoreRedux2: "0"
            }
          }
        }
      });
      getByText("Gaming Platform")
      expect(screen.getByText("Gaming Platform")).toBeVisible();
      // expect(screen.getByText("")).toBeVisible();
    } catch (e) {
      expect(e).toMatch('error');
    }
  });
});