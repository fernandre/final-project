import Leaderboards from "../../../components/home/Leaderboards";
import {render} from "@testing-library/react";
import "@testing-library/jest-dom";

beforeAll((done) => {
  done()
});
afterAll((done) => {
  done()
});
describe("Test <Leaderboards /> component rendering", () => {
  const leaderboards = [
    {
      game: "Rock Paper Scissors",
      total: 8,
      updated_at: "2022-04-16 20:00:00",
      username: "Test Acount edit"
    }
  ];
  const leaderboards_id = ["Cc10xtyd1IOUlKQZoqC26RDL3Q83"];
  test("Test", async () => {
    try {
      render(<Leaderboards leaderboards={leaderboards} leaderboards_id={leaderboards_id} />);
      // expect(screen.getByText("AST PLAYED GAMES")).toBeVisible();
    } catch (e) {
      expect(e).toMatch('error');
    }
  });
});
