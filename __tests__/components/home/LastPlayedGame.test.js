import LastPlayedGame from "../../../components/home/LastPlayedGame";
import {render} from "@testing-library/react";
import "@testing-library/jest-dom";

beforeAll((done) => {
  done()
});
afterAll((done) => {
  done()
});
describe("Test <LastPlayedGame /> component rendering", () => {
  const itemss = [
    {
      category: "Top Playing Game",
      developer: "Rockstar",
      genre: "RPG",
      name: "Assassin's Creed",
      release_date: "2022-01-01 08:00:00",
      thumbnail: "https://static.miraheze.org/awesomegameswiki/thumb/7/75/ACR.jpg/1200px-ACR.jpg"
    }
  ];
  const idss = ["3n76Nc0giOU6f7fKBoG6"];
  test("Test", async () => {
    try {
      render(<LastPlayedGame itemss={itemss} idss={idss} />);
      // expect(screen.getByText("LAST PLAYED GAMES")).toBeVisible();
    } catch (e) {
      expect(e).toMatch('error');
    }
  });
});
