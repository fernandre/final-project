import Updates from "../../../components/home/Updates";
import {render} from "@testing-library/react";
import "@testing-library/jest-dom";

beforeAll((done) => {
  done()
});
afterAll((done) => {
  done()
});
describe("Test <Updates /> component rendering", () => {
  const updates = [
    {
      description: "FIFA 22 adalah permainan simulasi video game bertema sepak bola yang akan datang dan diterbitkan oleh Electronic Arts sebagai bagian dari FIFA seri. Ini akan menjadi bagian ke-29 dalam seri FIFA, dan akan dirilis pada 1 Oktober 2021",
      game: "Fifa 22",
      release_date: "2022-03-04 15:30:00",
      thumbnail: "https://wallpaperaccess.com/full/1334461.jpg",
      version: "1.5"
    }
  ];
  const updates_id = ["CMH4pG1Eo2xSwfZyFjMf"];
  test("Test", async () => {
    try {
      render(<Updates updates={updates} updates_id={updates_id} activeIndex={0} animating={false} />);
      // expect(screen.getByRole("Button")).toBeVisible();
    } catch (e) {
      expect(e).toMatch('error');
    }
  });
});
