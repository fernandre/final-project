import Content from "../../../components/list/Content";
import {renderWithState} from "../../../redux/reduxtest/renderWithState";
import "@testing-library/jest-dom";

beforeAll((done) => {
  done()
});
afterAll((done) => {
  done()
});
describe("Test <Content /> component rendering", () => {
  test('Visible content', async () => {
    try {
      renderWithState(<Content />, {
        initialState: {
          auth: {
            initState: {
              isPlayed: 0
            }
          }
        }
      });
      // getByText("game Detail")
      // expect(screen.getAllByRole('div')).toHaveClass('container')
    } catch (e) {
      expect(e).toMatch('error');
    }
  });
});