import DetailList from "../../../components/list/DetailList";
import {render, screen} from "@testing-library/react";
import "@testing-library/jest-dom";

beforeAll((done) => {
  done()
});
afterAll((done) => {
  done()
});
describe("Test <DetailList /> component rendering", () => {
  test('Visible content', async () => {
    try {
      render(<DetailList />);
      expect(screen.getByText(
        "A game is a structured form of play, usually undertaken for entertainment or fun, and sometimes used as an educational tool. Games are different from work, which is usually carried out for remuneration, and from art, which is more often an expression of aesthetic or ideological elements."
      )).toBeVisible();
    } catch (e) {
      expect(e).toMatch('error');
    }
  });
});