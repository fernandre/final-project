import Gameselection from "../../../components/frontpage/Gameselection";
import {render, screen} from "@testing-library/react";
import "@testing-library/jest-dom";

beforeAll((done) => {
  done()
});
afterAll((done) => {
  done()
});
describe("Test <Gameselection /> component rendering", () => {
  test('Visible content', async () => {
    try {
      render(<Gameselection />);
      expect(screen.getByText("Game Detail")).toBeVisible();
    } catch (e) {
      expect(e).toMatch('error');
    }
  });
});