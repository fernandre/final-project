import Catagory from "../../../components/frontpage/Catagory";
import {render, screen} from "@testing-library/react";
import "@testing-library/jest-dom";

beforeAll((done) => {
  done()
});
afterAll((done) => {
  done()
});
describe("Test <Catagory /> component rendering", () => {
  test('Visible content', async () => {
    try {
      render(<Catagory />);
      expect(screen.getByText("Adventure")).toBeVisible();
    } catch (e) {
      expect(e).toMatch('error');
    }
  });
});