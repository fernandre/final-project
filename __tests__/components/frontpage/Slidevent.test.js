import Slidevent from "../../../components/frontpage/Slidevent";
import {render, screen} from "@testing-library/react";
import "@testing-library/jest-dom";

beforeAll((done) => {
  done()
});
afterAll((done) => {
  done()
});
describe("Test <Slidevent /> component rendering", () => {
  test('Visible content', async () => {
    try {
      render(<Slidevent />);
      expect(screen.getByText("Be the first to Play")).toBeVisible();
    } catch (e) {
      expect(e).toMatch('error');
    }
  });
});