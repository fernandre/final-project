import CardUser from "../../../components/games/CardUser";
import {render} from "@testing-library/react";
import "@testing-library/jest-dom";

beforeAll((done) => {
  done()
});
afterAll((done) => {
  done()
});
describe("Test <CardUser /> component rendering", () => {
  const detailGame =
  {
    category: "Top Playing Game",
    developer: "Rockstar",
    genre: "RPG",
    name: "Assassin's Creed",
    release_date: "2022-01-01 08:00:00",
    thumbnail: "https://static.miraheze.org/awesomegameswiki/thumb/7/75/ACR.jpg/1200px-ACR.jpg"
  }
  const gameId = "l9Ay2BQwtsJc7kfgfOp7"
  test("Test", async () => {
    try {
      render(<CardUser gameId={gameId} detailGame={detailGame} />);
      // expect(screen.getByText("")).toBeVisible();
    } catch (e) {
      expect(e).toMatch('error');
    }
  });
});