import Cardinfo from "../../../components/games/Cardinfo";
import {render} from "@testing-library/react";
import "@testing-library/jest-dom";

beforeAll((done) => {
  done()
});
afterAll((done) => {
  done()
});
describe("Test <Cardinfo /> component rendering", () => {
  const data =
    [{
      category: "Top Playing Game",
      developer: "Rockstar",
      genre: "RPG",
      name: "Assassin's Creed",
      release_date: "2022-01-01 08:00:00",
      thumbnail: "https://static.miraheze.org/awesomegameswiki/thumb/7/75/ACR.jpg/1200px-ACR.jpg"
    }]
  const gid = "l9Ay2BQwtsJc7kfgfOp7";
  test("Test", async () => {
    try {
      render(<Cardinfo detailGame2={data} gameId={gid} />);
      // expect(screen.getByText("")).toBeVisible();
    } catch (e) {
      expect(e).toMatch('error');
    }
  });
});