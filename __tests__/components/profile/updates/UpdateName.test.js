import UpdateName from "../../../../components/profile/updates/UpdateName";
import {fireEvent, render, screen} from "@testing-library/react";
import "@testing-library/jest-dom";

beforeAll((done) => {
  done()
});
afterAll((done) => {
  done()
});
describe("Test <UpdateName /> component rendering", () => {
  test("Try input form Username:'Admin', it should be the same change value", async () => {
    try {
      const utils = render(<UpdateName />);
      const inputUsername = utils.getByLabelText("username");
      fireEvent.change(inputUsername, {target: {value: "Admin"}})
      expect(inputUsername.value).toBe("Admin")
    } catch (e) {
      expect(e).toMatch('error');
    }
  });
  test("Try click button", async () => {
    try {
      const utils = render(<UpdateName />);
      const button = utils.getByRole("button");
      fireEvent.click(button)
      const rerender = render(<UpdateName />);
      const closeButton = rerender.getByLabelText("Close");
      fireEvent.click(closeButton)
      expect(screen.getAllByRole("button")).toBeDefined();
    } catch (e) {
      expect(e).toMatch('error');
    }
  });

});