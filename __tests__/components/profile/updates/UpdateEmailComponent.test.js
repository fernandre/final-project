import UpdateEmailComponent from "../../../../components/profile/updates/UpdateEmailComponent";
import {render} from "@testing-library/react";
import "@testing-library/jest-dom";

beforeAll((done) => {
  done()
});
afterAll((done) => {
  done()
});
describe("Test <UpdateEmailComponent /> component rendering", () => {
  test("Test", async () => {
    try {
      render(<UpdateEmailComponent />);
      // expect(screen.getByText("")).toBeVisible();
    } catch (e) {
      expect(e).toMatch('error');
    }
  });
});