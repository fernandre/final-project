import GameRPS from "../../../components/profile/GameRPS";
import {render, screen} from "@testing-library/react";
import "@testing-library/jest-dom";

beforeAll((done) => {
  done()
});
afterAll((done) => {
  done()
});
describe("Test <GameRPS /> component rendering", () => {
  test("Content text: 'Score Game Rock Paper Scissors'", async () => {
    try {
      render(<GameRPS />);
      expect(screen.getByText("Score Game Rock Paper Scissors")).toBeVisible();
    } catch (e) {
      expect(e).toMatch('error');
    }
  });
});