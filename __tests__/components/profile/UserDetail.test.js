import UserDetail from "../../../components/profile/UserDetail";
// import {screen} from "@testing-library/react";
import {renderWithState} from "../../../redux/reduxtest/renderWithState";
import "@testing-library/jest-dom";

beforeAll((done) => {
  done()
});
afterAll((done) => {
  done()
});
describe("Test <UserDetail /> component rendering", () => {
  const user =
  {
    uid: "N026CY1EomgrTV3N8PWJeFGSike2",
    displayName: "Admin",
    photoURL: "https://i.ibb.co/H4f3Hkv/profile.png",
    email: "admin@email.com"

  }
  test("Button class: 'btn btn-primary'", async () => {
    try {
      renderWithState(<UserDetail score={0} dataUser={user} />, {
        initialState: {
          auth: {
            initState: {
              scoreRedux: 0,
              scoreRedux2: 12
            }
          }
        }
      });
      // getByText("Update Profile")
      // expect(screen.getByRole('button')).toHaveClass('btn btn-primary')
    } catch (e) {
      expect(e).toMatch('error');
    }
  });
});