import Content from "../../../components/profile/Content";
import {render, screen} from "@testing-library/react";
import "@testing-library/jest-dom";

beforeAll((done) => {
  done()
});
afterAll((done) => {
  done()
});
describe("Test <Content /> component rendering", () => {
  test("Content text: 'Your Games'", async () => {
    try {
      render(<Content />);
      expect(screen.getByText("Your Games")).toBeVisible();
    } catch (e) {
      expect(e).toMatch('error');
    }
  });
});