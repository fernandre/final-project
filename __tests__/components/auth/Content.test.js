import Content from "../../../components/auth/Content";
import {render, screen} from "@testing-library/react";
// import {renderWithState} from "../../../redux/reduxtest/renderWithState";
import "@testing-library/jest-dom";

beforeAll((done) => {
  done()
});
afterAll((done) => {
  done()
});
describe("Test <Content /> component rendering", () => {
  test('Visible content', async () => {
    try {
      render(<Content />);
      expect(screen.getByText("Online Gaming Platform")).toBeVisible();
    } catch (e) {
      expect(e).toMatch('error');
    }
  });
});