import SignGoogle from "../../../components/auth/SignGoogle";
import {fireEvent, screen} from "@testing-library/react";
import {renderWithState} from "../../../redux/reduxtest/renderWithState";
import "@testing-library/jest-dom";

beforeAll((done) => {
  done()
});
afterAll((done) => {
  done()
});
describe("Test <SignGoogle /> component rendering", () => {
  test("Button class", async () => {
    try {
      const google = renderWithState(<SignGoogle />, {
        initialState: {
          auth: {
            initState: {
              buttonGoogle: "Google"
            }
          }
        }
      });
      const button = google.getByRole("button");
      fireEvent.click(button)
      // expect(button).toHaveTextContent("Process..");
      expect(screen.getByRole('button')).toHaveClass('btn mx-2 px-3')
    } catch (e) {
      expect(e).toMatch('error');
    }
  });
});