import SignFacebook from "../../../components/auth/SignFacebook";
import {fireEvent} from "@testing-library/react";
import {renderWithState} from "../../../redux/reduxtest/renderWithState";
import "@testing-library/jest-dom";

beforeAll((done) => {
  done()
});
afterAll((done) => {
  done()
});
describe("Test <SignFacebook /> component rendering", () => {
  test("Button class", async () => {
    try {
      const facebook = renderWithState(<SignFacebook />, {
        initialState: {
          auth: {
            initState: {
              buttonFacebook: "Facebook"
            }
          }
        }
      });
      const button = facebook.getByRole("button");
      fireEvent.click(button)
      // expect(button).toHaveTextContent("Process..");
      expect(button).toHaveClass('btn mx-2 px-2 btn btn-outline-secondary')
    } catch (e) {
      expect(e).toMatch('error');
    }
  });
});